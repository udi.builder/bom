#-------------------------------------------------
#
# Project created by QtCreator 2015-06-04T07:41:27
#
#-------------------------------------------------

QT  += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport concurrent

TARGET = altiumBOM
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    paint.cpp \
    cells.cpp \
    dialogtable.cpp \
    gost.cpp

HEADERS  += mainwindow.h \
    paint.h \
    cells.h \
    dialogtable.h \
    gost.h

FORMS    += mainwindow.ui

DISTFILES += \
    build/settings.ini \
    altiumBOM.rc

RESOURCES += \
    resource.qrc

RC_FILE = altiumBOM.rc
