#ifndef GOST_H
#define GOST_H

#include <QWidget>

class Gost : public QWidget
{
	Q_OBJECT
public:
	explicit Gost(QWidget *parent = 0);

protected:
	void paintEvent(QPaintEvent *);
signals:

public slots:
};

#endif // GOST_H
